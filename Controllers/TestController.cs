﻿using Microsoft.AspNetCore.Mvc;
namespace CRM.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TestController : ControllerBase
    {
        public IActionResult Test()
        {
            return Ok("Started!");
        }
    }
}
